#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* This code is public domain -- Will Hartung 4/9/09 */
size_t getlineAlternative(char **lineptr, size_t *n, FILE *stream) {
    char *bufptr = NULL;
    char *p = bufptr;
    size_t size;
    int c;

    if (lineptr == NULL) {
        return -1;
    }
    if (stream == NULL) {
        return -1;
    }
    if (n == NULL) {
        return -1;
    }
    bufptr = *lineptr;
    size = *n;

    c = fgetc(stream);
    if (c == EOF) {
        return -1;
    }
    if (bufptr == NULL) {
        bufptr = malloc(128);
        if (bufptr == NULL) {
            return -1;
        }
        size = 128;
    }
    p = bufptr;
    while(c != EOF) {
        if ((p - bufptr) > (size - 1)) {
            size = size + 128;
            bufptr = realloc(bufptr, size);
            if (bufptr == NULL) {
                return -1;
            }
        }
        *p++ = c;
        if (c == '\n') {
            break;
        }
        c = fgetc(stream);
    }

    *p++ = '\0';
    *lineptr = bufptr;
    *n = size;

    return p - bufptr - 1;
}

int main(int argc, char *argv[])
{ 
    double time_spent = 0.0;
    clock_t begin = clock();

    if (argc > 4) {
      printf("Number of parametes not ok.. I quit\n");
      exit(EXIT_FAILURE);
    }

    char *filename = argv[1];
    FILE * fp;
    char * line = NULL, *number;
    size_t len = 0;
    ssize_t read;
    int r, c;
    int nrows = atoi(argv[2]);
    int ncols = atoi(argv[3]);


   int i, j;
   float **mtx = (float **)malloc(nrows * sizeof(float *));

    for (i=0; i<nrows; i++)
         mtx[i] = (float *)malloc(ncols * sizeof(float));

    fp = fopen(filename, "r");
    if (fp == NULL) {
	     printf("error opening %s\n", argv[1]);
       exit(EXIT_FAILURE);
    }

    c = 0;
    while ((read = getlineAlternative(&line, &len, fp)) != -1) {
        r = 0;
        if (r == ncols || c == nrows) {
          printf("file not matching given num of rows or cols\n");
          exit(EXIT_FAILURE);
        }
        number = strtok(line, " ");
        while( number != NULL ) {
          mtx[c][r] = atof(number);
          number = strtok(NULL, " ");
          r++;
       }
        c++;
    }

    fclose(fp);
    if (line)
        free(line);

    fp = fopen("output.txt", "w");
    fputs("Indexes of columns where the number of negative elements is twice as large as the number of zeros.\n", fp);
    for (i = 0; i <  ncols; i++) {
      int negativeCount = 0;
      int zeroCount = 0;
      // printf("col %d\n", i);
      for (j = 0; j < nrows; j++) {
        if (mtx[j][i] < 0) {
          negativeCount++;
        }
        if (mtx[j][i] == 0) {
          zeroCount++;
        }
      }
      if (zeroCount * 2 == negativeCount) {
        fprintf(fp, "col %d\n", i);
      };
    }
    fclose(fp);
    free(mtx);
    printf("Output file successfully created\n");

    clock_t end = clock();
 
    // calculate elapsed time by finding difference (end - begin) and
    // dividing the difference by CLOCKS_PER_SEC to convert to seconds
    time_spent += (double)(end - begin) / CLOCKS_PER_SEC;
 
    printf("The elapsed time is %f seconds \n", time_spent);

    exit(EXIT_SUCCESS);
}